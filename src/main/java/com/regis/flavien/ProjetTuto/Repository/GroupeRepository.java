package com.regis.flavien.ProjetTuto.Repository;

import com.regis.flavien.ProjetTuto.Entities.Groupe;
import com.regis.flavien.ProjetTuto.Entities.Personne;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface GroupeRepository  extends JpaRepository<Groupe, Long> {

    @Query("SELECT p.id, p.nom, p.prenom, p.age FROM Personne p WHERE p.groupe = ?2")
    List<Personne> findAllPersonneByIdGroupe(Pageable pageable, Long id);
}
