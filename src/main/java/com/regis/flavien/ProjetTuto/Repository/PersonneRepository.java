package com.regis.flavien.ProjetTuto.Repository;

import com.regis.flavien.ProjetTuto.Entities.Personne;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface PersonneRepository extends JpaRepository<Personne, Long> {

    List<Personne> findByNomStartingWith(String nom, Pageable pageable);
}
