package com.regis.flavien.ProjetTuto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetTutoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetTutoApplication.class, args);
	}

}
