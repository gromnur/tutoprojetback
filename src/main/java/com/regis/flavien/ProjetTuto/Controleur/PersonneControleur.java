//package com.regis.flavien.ProjetTuto.Controleur;
//
//import com.regis.flavien.ProjetTuto.Entities.Personne;
//import com.regis.flavien.ProjetTuto.Repository.PersonneRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Pageable;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
///**
// * Controleur REST pour les personnes
// */
//@RestController
//@RequestMapping("/personne")
//public class PersonneControleur {
//
//    @Autowired
//    PersonneRepository personneRepository;
//
//    /**
//     * Recupere toutes les personnes
//     * @return La liste de tout les personnes
//     */
//    @GetMapping
//    public List<Personne> getAllPersonne(Pageable pageable) {
//        List<Personne> personnes = new ArrayList<>();
//        personneRepository.findAll(pageable).forEach(personnes::add);
//        return personnes;
//    }
//
//    /**
//     * Recupere une personne
//     * @param id L'id de la personne
//     * @return La personne
//     */
//    @GetMapping("{id}")
//    public Personne getPersonneById(@PathVariable("id") Long id) {
//        return personneRepository.findById(id).orElseThrow(() -> new IllegalStateException(
//                "L'étudiant " + id + " n'éxiste pas"
//        ));
//    }
//
//    /**
//     * Recupère les personnes qui commence par le paramètre
//     * @param nom L'id de la personne
//     * @return Liste des personne
//     */
//    @GetMapping("{nom}")
//    public List<Personne> getPersonneByNom(@PathVariable("nom") String nom, Pageable pageable) {
//        return personneRepository.findByNomStartingWith(nom, pageable);
//    }
//
//    /**
//     * Recupere toutes les personnes
//     * @return La liste de tout les personnes
//     */
//    @GetMapping("/countAll")
//    public long getCountPersonnes(Pageable pageable) {
//        List<Personne> personnes = new ArrayList<>();
//        return personneRepository.count();
//    }
//
//}
