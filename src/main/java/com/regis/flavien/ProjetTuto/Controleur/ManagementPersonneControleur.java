package com.regis.flavien.ProjetTuto.Controleur;

import com.regis.flavien.ProjetTuto.Entities.Personne;
import com.regis.flavien.ProjetTuto.Repository.PersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Controleur REST pour les personnes
 */
@RestController
@RequestMapping("/management/personne")
public class ManagementPersonneControleur {

    @Autowired
    PersonneRepository personneService;

    /**
     * Recupere toutes les personnes
     * @return La liste de tout les personnes
     */
    @GetMapping
    public List<Personne> getAllPersonne(Pageable pageable) {
        List<Personne> personnes = new ArrayList<>();
        personneService.findAll(pageable).forEach(personnes::add);
        return personnes;
    }

    /**
     * Recupere une personne
     * @param id L'id de la personne
     * @return La personne
     */
    @GetMapping("{id}")
    public Personne getPersonneById(@PathVariable("id") Long id) {
        Optional<Personne> oPers = personneService.findById(id);
        return oPers.orElseThrow(() -> new IllegalStateException(
                "L'étudiant " + id + " n'éxiste pas"
        ));
    }

    /**
     * Recupère les personnes qui commence par le paramètre
     * @param nom L'id de la personne
     * @return Liste des personne
     */
    @GetMapping("{nom}")
    public List<Personne> getPersonneByNom(@PathVariable("nom") String nom, Pageable pageable) {
        return personneService.findByNomStartingWith(nom, pageable);
    }

    /**
     * Ajoute une personne
     * @param personne La personne a ajouter
     * @return Message si l'insertion est reussi
     */
    @PutMapping
    public void addPersonne(@RequestBody Personne personne){
        System.out.println("Information personne : " + personne.getNom() + " " + personne.getPrenom());
        personneService.save(personne);
    }

    /**
     * Ajoute une personne
     * @param personne La personne a ajouter
     * @return Message si l'insertion est reussi
     */
    @PostMapping
    public void updatePersonne(@RequestBody Personne personne){
        System.out.println("Information personne : " + personne.getNom() + " " + personne.getPrenom());
        personneService.save(personne);
    }

    /**
     * Supprime la personne si elle existe
     * @param personne La personne à supprimer
     * @return Un message pour informer de la suppression
     */
    @DeleteMapping
    public void deletePersonne(@RequestBody Personne personne) {
        personneService.delete(personne);
    }
}
