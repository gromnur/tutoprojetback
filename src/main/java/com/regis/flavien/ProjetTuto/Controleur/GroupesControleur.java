//package com.regis.flavien.ProjetTuto.Controleur;
//
//import com.regis.flavien.ProjetTuto.Entities.Groupe;
//import com.regis.flavien.ProjetTuto.Entities.Personne;
//import com.regis.flavien.ProjetTuto.Repository.GroupeRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Pageable;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
///**
// * Controleur REST pour les groupes
// */
//@RestController
//@RequestMapping("/groupe")
//public class GroupesControleur {
//
//    @Autowired
//    GroupeRepository groupeRepository;
//
//    /**
//     * Recupere toutes les groupes
//     * @return La liste de tous les groupes
//     */
//    @GetMapping
//    public List<Groupe> getAllGroupes(Pageable pageable) {
//        List<Groupe> groupes = new ArrayList<>();
//        System.out.println(pageable);
//        groupeRepository.findAll(pageable).forEach(groupes::add);
//        return groupes;
//    }
//
//    /**
//     * Recupere le groupe
//     * @return La liste de tous les groupes
//     */
//    @GetMapping("{groupe_id}")
//    public Groupe getGroupeById(@PathVariable("groupe_id") Long groupe_id) {
//        return groupeRepository.findById(groupe_id).orElseThrow(() -> new IllegalStateException(
//                "Le Groupe " + groupe_id + " n'éxiste pas"
//        ));
//    }
//
//    /**
//     * Recupere toutes les groupes
//     * @return La liste de tous les groupes
//     */
//    @GetMapping("/countAll")
//    public Long getCountGroupes() {
//        return groupeRepository.count();
//    }
//
//    /**
//     * Recupere toutes les groupes
//     * @return La liste de tous les groupes
//     */
//    @GetMapping("{groupe_id}/personnes")
//    public List<Personne> getAllPersonneByIdGroupe(Pageable pageable, @PathVariable("groupe_id") Long groupe_id) {
//        return groupeRepository.findAllPersonneByIdGroupe(pageable, groupe_id);
//    }
//}
